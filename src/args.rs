use clap::Parser;
use log::Level;

/// The best Vrubi related Discord audio bot
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
pub struct Args {
    /// The number of transparent shards that should be started
    #[arg(short, long, env, default_value_t = 1)]
    pub shards: u64,

    /// The Discord bot token used for authentication
    #[arg(short = 't', long, env)]
    pub discord_token: String,

    /// The id of the Discord application
    #[arg(short = 'a', long, env)]
    pub application_id: u64,

    /// The youtube-dl executable to use
    #[arg(long, env, default_value = "yt-dlp")]
    pub youtube_dl: String,

    /// Specify role name(s) that a user needs to have to be able to use the bot
    #[arg(short, long, env = "ROLE_NAME")]
    pub role_names: Vec<String>,

    /// The level of log messages to display
    #[arg(short, long, env, default_value_t = Level::Warn)]
    pub log_level: Level,
}
