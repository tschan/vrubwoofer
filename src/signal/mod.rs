#[cfg(unix)]
use unix::{self as os_impl};
#[cfg(windows)]
use windows::{self as os_impl};

use std::io;

mod unix;
mod windows;

pub async fn exit_signal() -> io::Result<Option<String>> {
    os_impl::exit_signal().await
}