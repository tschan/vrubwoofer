#![cfg(any(windows))]

use std::io;
use tokio::signal::windows::{ctrl_c, ctrl_break};

pub async fn exit_signal() -> io::Result<Option<String>> {
    let mut sig_ctrl_c = ctrl_c()?;
    let mut sig_ctrl_break = ctrl_break()?;
    Ok(Some(tokio::select! {
        _ = sig_ctrl_c.recv() => String::from("CTRL-C"),
        _ = sig_ctrl_break.recv() => String::from("CTRL-BREAK"),
    }))
}
