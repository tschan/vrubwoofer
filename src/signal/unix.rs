#![cfg(any(unix))]

use std::io;
use tokio::signal::unix::{signal, SignalKind};

pub async fn exit_signal() -> io::Result<Option<String>> {
    let mut sigint = signal(SignalKind::interrupt())?;
    let mut sigterm = signal(SignalKind::terminate())?;
    Ok(tokio::select! {
        v = sigint.recv() => v.map(|_| String::from("SIGINT")),
        v = sigterm.recv() => v.map(|_| String::from("SIGTERM")),
    })
}