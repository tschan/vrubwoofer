use std::sync::Arc;

use serenity::prelude::TypeMapKey;

pub struct AllowedRoles;

impl TypeMapKey for AllowedRoles {
    type Value = Arc<Vec<String>>;
}

pub struct YoutubeDl;

impl TypeMapKey for YoutubeDl {
    type Value = String;
}
