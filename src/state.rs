use std::cmp::{max, min};
use std::error::Error as StdError;
use std::sync::Arc;
use std::time::Duration;

use comfy_table::{ContentArrangement, Table};
use comfy_table::presets::ASCII_MARKDOWN;
use dashmap::DashMap;
use serenity::async_trait;
use serenity::model::id::{ChannelId, GuildId};
use serenity::prelude::TypeMapKey;
use songbird::{Call, CoreEvent, Event, EventContext, EventHandler, input, Songbird, TrackEvent};
use songbird::input::Metadata;
use tokio::sync::Mutex;

use crate::commands::base::Error;

const MAX_TABLE_WIDTH: u16 = 104;

type InnerStateType = Arc<Mutex<InnerState>>;

#[derive(Debug, Clone)]
pub struct State {
    guild_id: GuildId,
    state: InnerStateType
}

impl State {
    pub fn new(guild_id: GuildId, manager: Arc<Songbird>) -> Self {
        State {
            guild_id,
            state: Arc::new(Mutex::new(InnerState::new(manager)))
        }
    }

    pub fn get_guild_id(&self) -> GuildId {
        self.guild_id
    }

    pub async fn leave(&mut self) -> Result<(), Box<dyn StdError + Send + Sync>> {
        let mut state = self.state.as_ref().lock().await;
        state.leave().await
    }

    pub async fn queue_tracks(&mut self, tracks: Vec<Metadata>, position: Option<isize>) -> Result<usize, Box<dyn StdError + Send + Sync>> {
        let mut state = self.state.as_ref().lock().await;
        state.queue_tracks(tracks, position)
    }

    pub async fn clear_queue(&mut self) {
        let mut state = self.state.as_ref().lock().await;
        state.stop().await;
        state.clear_queue()
    }

    pub async fn seek(&mut self, channel_id: ChannelId, position: usize, timeout: Option<Duration>) -> Result<(), Box<dyn StdError + Send + Sync>> {
        let mut state = self.state.as_ref().lock().await;
        state.join_channel(self.state.clone(), self.guild_id.clone(), channel_id).await?;
        state.play(position, timeout).await
    }

    pub async fn play(&mut self, channel_id: ChannelId, tracks: Vec<Metadata>) -> Result<(), Box<dyn StdError + Send + Sync>> {
        let mut state = self.state.as_ref().lock().await;
        state.clear_queue();
        state.queue_tracks(tracks, None)?;
        state.join_channel(self.state.clone(), self.guild_id.clone(), channel_id).await?;
        state.play(0, None).await
    }

    pub async fn stop(&mut self) {
        let mut state = self.state.as_ref().lock().await;
        state.stop().await
    }

    pub async fn len(&self) -> usize {
        let state = self.state.as_ref().lock().await;
        state.len()
    }

    pub async fn to_string(&self, window: usize, first_track: Option<isize>) -> String {
        let state = self.state.as_ref().lock().await;
        state.to_string(window, first_track)
    }
}

#[derive(Debug)]
struct InnerState {
    manager: Arc<Songbird>,
    handler: Option<Arc<Mutex<Call>>>,
    current_track: Option<usize>,
    playlist: Vec<Metadata>
}

impl InnerState {
    fn new(manager: Arc<Songbird>) -> Self {
        InnerState {
            manager,

            handler: None,
            current_track: None,
            playlist: vec![]
        }
    }

    async fn join_channel(&mut self, state: InnerStateType, guild_id: GuildId, channel_id: ChannelId) -> Result<(), Box<dyn StdError + Send + Sync>> {
        if self.handler.is_some() {
            let mut handler = self.handler.as_ref().unwrap().lock().await;
            if handler.current_channel().map_or(true, |v| v.0 != channel_id.0) {
                handler.join(ChannelId::from(channel_id)).await?;
            }
        } else {
            let (handle_lock, success) = self.manager.join(guild_id.clone(), channel_id).await;
            if let Err(e) = success {
                return Err(e.into());
            }
            {
                let mut handler = handle_lock.lock().await;
                handler.add_global_event(
                    Event::Core(CoreEvent::DriverDisconnect),
                    DisconnectWatchdog {
                        state: state.clone(),
                        guild_id: guild_id.clone()
                    }
                );
                handler.add_global_event(
                    Event::Track(TrackEvent::End),
                    QueueHandler {
                        state,
                        guild_id: guild_id.clone()
                    }
                );
            }
            self.handler = Some(handle_lock);
        }
        Ok(())
    }

    async fn leave(&mut self) -> Result<(), Box<dyn StdError + Send + Sync>> {
        if self.handler.is_some() {
            let mut handler = self.handler.as_ref().unwrap().lock().await;
            handler.leave().await?
        }
        Ok(())
    }

    fn queue_tracks(&mut self, tracks: Vec<Metadata>, position: Option<isize>) -> Result<usize, Box<dyn StdError + Send + Sync>> {
        let index = match position {
            None => self.playlist.len(),
            Some(pos) if pos < 0 => max(0, self.playlist.len()
                .checked_sub(usize::try_from(pos.abs())
                    .unwrap_or(usize::MAX)
                    .checked_sub(1)
                    .unwrap_or(0)
                ).unwrap_or(0)),
            Some(pos) => min(self.playlist.len(), usize::try_from(pos)?)
        };
        // if we add the new tracks at the position of the currently playing track we need to update
        // the current track information accordingly
        if let Some(current_track) = self.current_track {
            if index == current_track {
                self.current_track = Some(current_track + tracks.len());
            }
        }
        self.playlist.splice(index..index, tracks);
        Ok(index)
    }

    fn clear_queue(&mut self) {
        self.playlist.clear();
        self.current_track = None;
    }

    async fn play(&mut self, position: usize, timeout: Option<Duration>) -> Result<(), Box<dyn StdError + Send + Sync>> {
        if position >= self.playlist.len() {
            return Err(Error::Generic("There is no track for the given position".into()).into());
        }

        let url = self.playlist.get(position).ok_or(Error::Internal("Could not get track from playlist".to_string()))?;
        let mut handler = self.handler.as_ref().ok_or(Error::Internal("No handler for state".to_string()))?.lock().await;

        let url = url.source_url.as_ref().ok_or(Error::Internal("Track has no source url".to_string()))?;
        let source_future = input::ytdl(url);

        let source = match timeout {
            Some(timeout) => tokio::time::timeout(timeout, source_future).await??,
            None => source_future.await?
        };

        handler.play_only_source(source);
        self.current_track = Some(position);
        Ok(())
    }

    async fn stop(&mut self) {
        if let Some(handler_lock) = &self.handler {
            let mut handler = handler_lock.lock().await;
            handler.stop();
            self.current_track = None;
        }
    }

    fn len(&self) -> usize {
        return self.playlist.len();
    }

    fn to_string(&self, window: usize, first_track: Option<isize>) -> String {
        let string = if !self.playlist.is_empty() {
            let window = max(window, 2);
            let mut table = Table::new();
            table
                .load_preset(ASCII_MARKDOWN)
                .set_content_arrangement(ContentArrangement::Dynamic)
                .force_no_tty()
                .set_width(MAX_TABLE_WIDTH)
                .set_header(vec!["", "Pos", "Video", "Title"]);
            let playlist_len = self.playlist.len();
            let (window, slice) = if playlist_len <= window {
                (&self.playlist[..], (0..playlist_len))
            } else {
                let mut start = first_track
                    .map(|v| {
                        if v < 0 {
                            max(0, playlist_len.checked_sub(usize::try_from(v.abs()).unwrap_or(usize::MAX)).unwrap_or(0))
                        } else {
                            min(playlist_len - 1, usize::try_from(v).unwrap_or(usize::MAX))
                        }
                    })
                    .or_else(|| self.current_track.unwrap_or(1).checked_sub(1))
                    .unwrap_or(0);
                let mut end = start + window;
                let overhang = end.checked_sub(playlist_len).unwrap_or(0);
                if overhang > 0 {
                    start = start.checked_sub(overhang).unwrap_or(0);
                    end = playlist_len;
                }
                (&self.playlist[start..end], (start..end))
            };
            if slice.start > 0 {
                table.add_row(vec!["", "...", "...", "..."]);
            }
            for (item, i) in window.iter().zip(slice.clone()) {
                let play_col = match self.current_track {
                    Some(j) if j == i => "|>",
                    None if i == 0 => "[]",
                    _ => ""
                };
                table.add_row(vec![
                    &play_col.to_string(),
                    &i.to_string(),
                    item.source_url.as_ref().expect("Source url will be set"),
                    item.title.as_ref().unwrap_or(&"".to_string())
                ]);
            }
            if slice.end < playlist_len {
                table.add_row(vec!["", "...", "...", "..."]);
            }
            table.to_string()
        } else {
            "Nothing queued yet :(".to_string()
        };
        return string;
    }
}

struct DisconnectWatchdog {
    state: InnerStateType,
    guild_id: GuildId
}

#[async_trait]
impl EventHandler for DisconnectWatchdog {
    async fn act(&self, ctx: &EventContext<'_>) -> Option<Event> {
        match ctx {
            EventContext::DriverDisconnect(_) => {
                log::warn!("Driver for {} was disconnected, stopping any tracks that might still be playing", self.guild_id);
                let mut state = self.state.lock().await;
                state.stop().await;
            }
            _ => log::warn!("Unexpected event in DisconnectWatchdog: {:?}", ctx)
        }

        None
    }
}

struct QueueHandler {
    state: InnerStateType,
    guild_id: GuildId
}

#[async_trait]
impl EventHandler for QueueHandler {
    async fn act(&self, ctx: &EventContext<'_>) -> Option<Event> {
        match ctx {
            EventContext::Track(_) => {
                let mut state = self.state.lock().await;
                while state.current_track.is_some() {
                    let new_track = state.current_track.unwrap() + 1usize;
                    if new_track >= state.playlist.len() {
                        // end of playlist reached
                        state.stop().await;
                    } else {
                        match state.play(new_track, None).await {
                            Ok(_) => break,
                            Err(why) => {
                                log::warn!("Playback of next track for {} failed: {:?}", self.guild_id, why);
                                // we still need to update the current_track value otherwise the loop
                                // won't progress
                                state.current_track = Some(new_track);
                            }
                        }
                    }
                }
            }

            _ => log::warn!("Unexpected event in QueueHandler: {:?}", ctx)
        }
        None
    }
}

type PlayStateMap = Arc<DashMap<GuildId, State>>;

pub struct PlayState;

impl TypeMapKey for PlayState {
    type Value = PlayStateMap;
}
