use std::error::Error;
use std::process::Stdio;
use std::time::Duration;

use songbird::input::Metadata;
use tokio::process::Command;

pub async fn ytdl_metadata(youtube_dl: &str, uri: &str, timeout: Option<Duration>) -> Result<Vec<Metadata>, Box<dyn Error + Send + Sync>> {
    let ytdl_args = [
        "-j",
        "-R",
        "infinite",
        "--ignore-config",
        "--no-warnings",
        uri
    ];

    let youtube_dl_cmd = Command::new(youtube_dl)
        .args(&ytdl_args)
        .stdin(Stdio::null())
        .output();

    let youtube_dl_output = match timeout {
        Some(timeout) => tokio::time::timeout(timeout, youtube_dl_cmd).await??,
        None => youtube_dl_cmd.await?
    };

    let mut result = Vec::new();

    let o_vec = youtube_dl_output.stdout;

    for s in o_vec.split(|el| *el == 0xA) {
        if !s.is_empty() {
            let value = serde_json::from_slice(s)?;
            let metadata = Metadata::from_ytdl_output(value);
            if let Some(_) = metadata.source_url {
                result.push(metadata)
            }
        }
    }

    Ok(result)
}

pub async fn ytdl_version(youtube_dl: &str) -> Result<String, Box<dyn Error + Send + Sync>> {
    let ytdl_args = [
        "--version"
    ];

    let youtube_dl_cmd = Command::new(youtube_dl)
        .args(&ytdl_args)
        .stdin(Stdio::null())
        .output();

    let youtube_dl_output = youtube_dl_cmd.await?;

    let version_string = String::from_utf8(youtube_dl_output.stdout)?;
    Ok(version_string)
}
