use serenity::async_trait;
use serenity::builder::CreateApplicationCommands;
use serenity::client::Context;
use serenity::model::application::interaction::application_command::ApplicationCommandInteraction;
use serenity::model::prelude::command::CommandOptionType;
use serenity::model::prelude::interaction::application_command::CommandDataOptionValue;
use serenity::utils::{content_safe, ContentSafeOptions};

use crate::commands::base::{Command, CommandHelper, CommandResult, Error};
use crate::config::YoutubeDl;
use crate::ytdl::ytdl_metadata;

pub struct QueueCommand {}

#[async_trait]
impl Command for QueueCommand {
    fn create_application_command(&self, commands: &mut CreateApplicationCommands) {
        commands
            .create_application_command(|command| {
                command
                    .name(QueueCommand::name())
                    .description("Add a new video to the playlist")
                    .create_option(|option| {
                        option
                            .name("url")
                            .description("The url to add")
                            .kind(CommandOptionType::String)
                            .required(true)
                    })
                    .create_option(|option| {
                        option
                            .name("position")
                            .description("The position in the playlist to add it too")
                            .kind(CommandOptionType::Integer)
                    })
            });
    }

    async fn create_interaction(&self, ctx: &Context, command: &ApplicationCommandInteraction) -> CommandResult<Option<String>> {
        let url = match command.data.options
            .iter()
            .find(|&v| v.name == "url")
            .ok_or::<Error>("Url parameter not given".into())?
            .resolved
            .as_ref()
            .ok_or::<Error>("Url parameter cannot be resolved".into())? {
            CommandDataOptionValue::String(url) => url,
            _ => {
                return Err(Error::Internal("Unexpected option value type".to_string()).into())
            }
        };

        let position = match command.data.options
            .iter()
            .find(|&v| v.name == "position")
            .and_then(|v| v.resolved.as_ref()) {
            Some(CommandDataOptionValue::Integer(v)) => Some(isize::try_from(v.clone())?),
            _ => None,
        };

        let youtube_dl = {
            let data = ctx.data.read().await;
            data.get::<YoutubeDl>().expect("YoutubeDl should be set").clone()
        };

        let tracks = ytdl_metadata(&*youtube_dl, url.as_str(), None).await?;

        let mut state = CommandHelper::extract_state(ctx, command).await?;
        let tracks_len = tracks.len();
        let index = state.queue_tracks(tracks, position).await?;
        let message = format!("Added {} link(s) at position {}", tracks_len, index);
        let content = content_safe(&ctx.cache, message, &ContentSafeOptions::default(), &[]);
        Ok(Some(content))
    }
}

impl QueueCommand {
    pub(crate) fn name() -> String {
        "queue".to_string()
    }
}

