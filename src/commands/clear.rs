use serenity::async_trait;
use serenity::builder::CreateApplicationCommands;
use serenity::client::Context;
use serenity::model::application::interaction::application_command::ApplicationCommandInteraction;

use crate::commands::base::{Command, CommandHelper, CommandResult};

pub struct ClearCommand {}

#[async_trait]
impl Command for ClearCommand {
    fn create_application_command(&self, commands: &mut CreateApplicationCommands) {
        commands
            .create_application_command(|command| {
                command
                    .name(ClearCommand::name())
                    .description("Clear the playlist and stop the current playback")
            });
    }

    async fn create_interaction(&self, ctx: &Context, command: &ApplicationCommandInteraction) -> CommandResult<Option<String>> {
        let mut state = CommandHelper::extract_state(ctx, command).await?;
        state.clear_queue().await;
        Ok(None)
    }
}

impl ClearCommand {
    pub(crate) fn name() -> String {
        "clear".to_string()
    }
}
