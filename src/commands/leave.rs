use serenity::async_trait;
use serenity::builder::CreateApplicationCommands;
use serenity::client::Context;
use serenity::model::application::interaction::application_command::ApplicationCommandInteraction;

use crate::commands::base::{Command, CommandHelper, CommandResult};

pub struct LeaveCommand {}

#[async_trait]
impl Command for LeaveCommand {
    fn create_application_command(&self, commands: &mut CreateApplicationCommands) {
        commands
            .create_application_command(|command| {
                command
                    .name(LeaveCommand::name())
                    .description("Leave the voice channel (if in any currently)")
            });
    }

    async fn create_interaction(&self, ctx: &Context, command: &ApplicationCommandInteraction) -> CommandResult<Option<String>> {
        let mut state = CommandHelper::extract_state(ctx, command).await?;
        state.leave().await?;
        Ok(None)
    }
}

impl LeaveCommand {
    pub(crate) fn name() -> String {
        "leave".to_string()
    }
}
