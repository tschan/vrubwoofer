use build_time::build_time_utc;
use git_version::git_version;
use serenity::async_trait;
use serenity::builder::CreateApplicationCommands;
use serenity::client::Context;
use serenity::model::application::interaction::application_command::ApplicationCommandInteraction;
use serenity::utils::{content_safe, ContentSafeOptions, MessageBuilder};

use crate::commands::base::{Command, CommandResult};
use crate::config::YoutubeDl;
use crate::ytdl::ytdl_version;

pub struct VersionCommand {}

const NAME: &str = env!("CARGO_PKG_NAME");
const VERSION: &str = env!("CARGO_PKG_VERSION");
const GIT_VERSION: &str = git_version!(args = ["--always", "--dirty=-modified", "--long"]);
const BUILD_TIME: &str = build_time_utc!("%Y-%m-%dT%H:%M:%S%:z");

#[async_trait]
impl Command for VersionCommand {
    fn create_application_command(&self, commands: &mut CreateApplicationCommands) {
        commands
            .create_application_command(|command| {
                command
                    .name(VersionCommand::name())
                    .description("Print version information")
            });
    }

    async fn create_interaction(&self, ctx: &Context, _: &ApplicationCommandInteraction) -> CommandResult<Option<String>> {
        let youtube_dl = {
            let data = ctx.data.read().await;
            data.get::<YoutubeDl>().expect("YoutubeDl should be set").clone()
        };
        let youtube_dl_version = ytdl_version(&youtube_dl).await.unwrap_or_else(|_| "?".to_string());

        let mut message_builder = MessageBuilder::new();
        let version_string = format!("{} {} ({}) built on {}\n{} {}", NAME, VERSION, GIT_VERSION, BUILD_TIME, &youtube_dl, &youtube_dl_version);
        message_builder.push_codeblock(version_string, None);
        let content = content_safe(&ctx.cache, message_builder.to_string(), &ContentSafeOptions::default(),  &[]);
        Ok(Some(content))
    }
}

impl VersionCommand {
    pub(crate) fn name() -> String {
        "version".to_string()
    }
}
