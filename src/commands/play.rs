use serenity::async_trait;
use serenity::builder::CreateApplicationCommands;
use serenity::client::Context;
use serenity::model::application::interaction::application_command::ApplicationCommandInteraction;
use serenity::model::prelude::command::CommandOptionType;
use serenity::model::prelude::interaction::application_command::CommandDataOptionValue;

use crate::commands::base::{Command, CommandHelper, CommandResult, Error};
use crate::config::YoutubeDl;
use crate::ytdl::ytdl_metadata;

pub struct PlayCommand {}

#[async_trait]
impl Command for PlayCommand {
    fn create_application_command(&self, commands: &mut CreateApplicationCommands) {
        commands
            .create_application_command(|command| {
                command
                    .name(PlayCommand::name())
                    .description("Start playing a video immediately (overwriting any existing playlist)")
                    .create_option(|option| {
                        option
                            .name("url")
                            .description("The url to add")
                            .kind(CommandOptionType::String)
                            .required(true)
                    })
            });
    }

    async fn create_interaction(&self, ctx: &Context, command: &ApplicationCommandInteraction) -> CommandResult<Option<String>> {
        let url = match command.data.options
            .iter()
            .find(|&v| v.name == "url")
            .ok_or::<Error>("Url parameter not given".into())?
            .resolved
            .as_ref()
            .ok_or::<Error>("Url parameter cannot be resolved".into())? {
            CommandDataOptionValue::String(url) => url,
            _ => {
                return Err(Error::Internal("Unexpected option value type".to_string()).into())
            }
        };

        let youtube_dl = {
            let data = ctx.data.read().await;
            data.get::<YoutubeDl>().expect("YoutubeDl should be set").clone()
        };

        let tracks = ytdl_metadata(&*youtube_dl, url.as_str(), None).await?;

        if tracks.is_empty() {
            return Err(Error::Generic("Could not extract any video from the given url".to_string()).into());
        }

        let mut state = CommandHelper::extract_state(ctx, command).await?;

        let channel_id = CommandHelper::extract_channel_id(ctx, command, &state).await?;

        let tracks_len = tracks.len();
        state.play(channel_id, tracks).await?;
        Ok(Some(format!("Added {} link(s) to play immediately", tracks_len).to_string()))
    }
}

impl PlayCommand {
    pub(crate) fn name() -> String {
        "play".to_string()
    }
}
