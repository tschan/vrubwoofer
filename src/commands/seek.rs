use serenity::async_trait;
use serenity::builder::CreateApplicationCommands;
use serenity::client::Context;
use serenity::model::application::interaction::application_command::ApplicationCommandInteraction;
use serenity::model::prelude::command::CommandOptionType;
use serenity::model::prelude::interaction::application_command::CommandDataOptionValue;

use crate::commands::base::{Command, CommandHelper, CommandResult, Error};

pub struct SeekCommand {}

#[async_trait]
impl Command for SeekCommand {
    fn create_application_command(&self, commands: &mut CreateApplicationCommands) {
        commands
            .create_application_command(|command| {
                command
                    .name(SeekCommand::name())
                    .description("Seek to a specific song in the playlist")
                    .create_option(|option| {
                        option
                            .name("position")
                            .description("The song to play")
                            .kind(CommandOptionType::Integer)
                            .required(true)
                    })
            });
    }

    async fn create_interaction(&self, ctx: &Context, command: &ApplicationCommandInteraction) -> CommandResult<Option<String>> {
        let position = match command.data.options
            .iter()
            .find(|&v| v.name == "position")
            .ok_or::<Error>("Position parameter not given".into())?
            .resolved
            .as_ref()
            .ok_or::<Error>("Position parameter cannot be resolved".into())? {
            CommandDataOptionValue::Integer(position) => usize::try_from(position.clone())?,
            _ => {
                return Err(Error::Internal("Unexpected option value type".to_string()).into())
            }
        };

        let mut state = CommandHelper::extract_state(ctx, command).await?;
        if position < state.len().await {
            let channel_id = CommandHelper::extract_channel_id(ctx, command, &state).await?;
            state.seek(channel_id, position, None).await?;
            Ok(None)
        } else {
            Err(Error::Generic("I don't have that many tracks in my playlist :(".to_string()).into())
        }
    }
}

impl SeekCommand {
    pub(crate) fn name() -> String {
        "seek".to_string()
    }
}

