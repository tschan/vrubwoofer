use std::error::Error as StdError;
use std::fmt::{Display, Formatter};

use dashmap::mapref::entry::Entry;
use serenity::async_trait;
use serenity::builder::CreateApplicationCommands;
use serenity::client::Context;
use serenity::model::application::interaction::application_command::ApplicationCommandInteraction;

use crate::config::AllowedRoles;
use crate::state::{PlayState, State};
use serenity::model::id::ChannelId;

pub type CommandError = Box<dyn StdError + Send + Sync>;
pub type CommandResult<T = ()> = Result<T, CommandError>;

#[derive(Debug)]
pub enum Error {
    Generic(String),
    Internal(String),
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Error::Generic(inner) => write!(f, "{}", inner),
            Error::Internal(_) => write!(f, "Something broke really badly :(")
        }
    }
}

impl StdError for Error {}

impl From<&str> for Error {
    fn from(val: &str) -> Self {
        Error::Generic(val.to_string())
    }
}

#[async_trait]
pub trait Command: Sync {
    fn create_application_command(&self, commands: &mut CreateApplicationCommands);
    async fn create_interaction(&self, ctx: &Context, command: &ApplicationCommandInteraction) -> CommandResult<Option<String>>;

    async fn can_execute(&self, ctx: &Context, command: &ApplicationCommandInteraction) -> bool {
        let allowed_roles = {
            let data = ctx.data.read().await;
            data.get::<AllowedRoles>().expect("Allowed Roles should be set").clone()
        };

        let mut is_admin = false;
        if let Some(member) = &command.member {
            for role in &member.roles {
                if let Some(r) = role
                    .to_role_cached(&ctx.cache)
                {
                    for check_role in allowed_roles.as_ref() {
                        if &r.name == check_role {
                            is_admin = true;
                            break;
                        }
                    }
                }
            }
        }
        is_admin
    }
}

pub struct CommandHelper;

impl CommandHelper {
    pub async fn extract_state(ctx: &Context, command: &ApplicationCommandInteraction) -> CommandResult<State> {
        let guild_id = command.guild_id
            .ok_or(Error::Generic("You need to message me from within a guild channel, otherwise I won't know where to play stuff :(".to_string()))?;
        let play_states = {
            let data = ctx.data.read().await;
            data.get::<PlayState>().ok_or(Error::Internal("Play states should be set".to_string()))?.clone()
        };
        let entry = play_states.entry(guild_id);
        let state = match entry {
            Entry::Occupied(entry) => entry.get().clone(),
            Entry::Vacant(entry) => {
                let manager = songbird::get(ctx).await
                    .ok_or(Error::Internal("No Songbird instance found o.O".to_string()))?;
                entry.insert(State::new(guild_id, manager)).value().clone()
            }
        };
        Ok(state)
    }

    pub async fn extract_channel_id(ctx:&Context, command: &ApplicationCommandInteraction, state: &State) -> CommandResult<ChannelId> {
        let channel_id = state.get_guild_id()
            .to_guild_cached(&ctx.cache).ok_or::<Error>("No guild found!".into())?
            .voice_states
            .get(&command.user.id)
            .and_then(|voice_state| voice_state.channel_id)
            .ok_or::<Error>("You are not in any voice channel!".into())?;
        Ok(channel_id)
    }
}