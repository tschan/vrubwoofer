use serenity::async_trait;
use serenity::builder::CreateApplicationCommands;
use serenity::client::Context;
use serenity::model::application::interaction::application_command::ApplicationCommandInteraction;
use serenity::model::prelude::command::CommandOptionType;
use serenity::model::prelude::interaction::application_command::CommandDataOptionValue;
use serenity::utils::{content_safe, ContentSafeOptions, MessageBuilder};

use crate::commands::base::{Command, CommandHelper, CommandResult};

const WINDOW_SIZE: usize = 5;

pub struct StatusCommand {}

#[async_trait]
impl Command for StatusCommand {
    fn create_application_command(&self, commands: &mut CreateApplicationCommands) {
        commands
            .create_application_command(|command| {
                command.name(StatusCommand::name())
                    .description("Retrieve the current state of the queue")
                    .create_option(|option| {
                        option
                            .name("window")
                            .description(format!("The number of tracks to display at once (default: {})", WINDOW_SIZE))
                            .kind(CommandOptionType::Integer)
                            .required(false)
                    })
                    .create_option(|option| {
                        option
                            .name("position")
                            .description("The the first track to display (default: current track minus 1)")
                            .kind(CommandOptionType::Integer)
                            .required(false)
                    })
            });
    }

    async fn create_interaction(&self, ctx: &Context, command: &ApplicationCommandInteraction) -> CommandResult<Option<String>> {
        let window = match command.data.options
            .iter()
            .find(|&v| v.name == "window")
            .and_then(|v| v.resolved.as_ref()) {
            Some(CommandDataOptionValue::Integer(window)) => usize::try_from(window.clone()).unwrap_or(WINDOW_SIZE),
            _ => WINDOW_SIZE
        };
        let first_track = match command.data.options
            .iter()
            .find(|&v| v.name == "position")
            .and_then(|v| v.resolved.as_ref()) {
            Some(CommandDataOptionValue::Integer(first_track)) => isize::try_from(first_track.clone()).ok(),
            _ => None
        };

        let state = CommandHelper::extract_state(ctx, command).await?;
        let mut state_string = state.to_string(window, first_track).await;
        if state_string.len() > 1992 {
            state_string = state_string[..1988].to_string() + "\n...";
        }
        let mut message_builder = MessageBuilder::new();
        message_builder.push_codeblock(state_string, None);
        let content = content_safe(&ctx.cache, message_builder.to_string(), &ContentSafeOptions::default(),  &[]);
        Ok(Some(content))
    }
}

impl StatusCommand {
    pub(crate) fn name() -> String {
        "status".to_string()
    }
}
