use serenity::async_trait;
use serenity::builder::CreateApplicationCommands;
use serenity::client::Context;
use serenity::model::application::interaction::application_command::ApplicationCommandInteraction;

use crate::commands::base::{Command, CommandHelper, CommandResult};

pub struct StopCommand {}

#[async_trait]
impl Command for StopCommand {
    fn create_application_command(&self, commands: &mut CreateApplicationCommands) {
        commands
            .create_application_command(|command| {
                command
                    .name(StopCommand::name())
                    .description("Stop the current playback")
            });
    }

    async fn create_interaction(&self, ctx: &Context, command: &ApplicationCommandInteraction) -> CommandResult<Option<String>> {
        let mut state = CommandHelper::extract_state(ctx, command).await?;
        state.stop().await;
        Ok(None)
    }
}

impl StopCommand {
    pub(crate) fn name() -> String {
        "stop".to_string()
    }
}
