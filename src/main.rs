use std::collections::HashMap;
use std::process::Stdio;
use std::sync::Arc;
use std::time::Duration;

use clap::Parser;
use dashmap::DashMap;
use lazy_static::lazy_static;
use log::{debug, error, info, warn};
use serenity::async_trait;
use serenity::client::{Client, Context, EventHandler};
use serenity::futures::pin_mut;
use serenity::model::application::command::Command as SerenityCommand;
use serenity::model::application::interaction::{Interaction, InteractionResponseType};
use serenity::model::gateway::Ready;
use serenity::prelude::GatewayIntents;
use songbird::SerenityInit;

use state::PlayState;

use crate::commands::{clear::ClearCommand, leave::LeaveCommand, play::PlayCommand, queue::QueueCommand, seek::SeekCommand, status::StatusCommand, stop::StopCommand};
use crate::commands::base::{Command, Error};
use crate::commands::version::VersionCommand;
use crate::config::{AllowedRoles, YoutubeDl};

mod args;
mod commands;
mod state;
mod config;
mod ytdl;
mod signal;

const COMMAND_TIMEOUT: Duration = Duration::from_millis(2500);

lazy_static! {
    static ref COMMANDS: HashMap<String, Box<dyn Command + Sync + Send>> = {
        let mut m: HashMap<String, Box<dyn Command + Sync + Send>> = HashMap::new();
        m.insert(StatusCommand::name(), Box::new(StatusCommand {}));
        m.insert(QueueCommand::name(), Box::new(QueueCommand {}));
        m.insert(PlayCommand::name(), Box::new(PlayCommand {}));
        m.insert(StopCommand::name(), Box::new(StopCommand {}));
        m.insert(LeaveCommand::name(), Box::new(LeaveCommand {}));
        m.insert(SeekCommand::name(), Box::new(SeekCommand {}));
        m.insert(ClearCommand::name(), Box::new(ClearCommand {}));
        m.insert(VersionCommand::name(), Box::new(VersionCommand {}));
        m
    };
}

struct Handler;

#[async_trait]
impl EventHandler for Handler {
    async fn ready(&self, ctx: Context, ready: Ready) {
        info!("{} is connected! (shard: {})", ready.user.name, ctx.shard_id);

        let commands = SerenityCommand::set_global_application_commands(&ctx.http, |commands| {
            for c in COMMANDS.values() {
                c.create_application_command(commands);
            }
            commands
        }).await.expect("Could not create application commands");

        debug!("Registered the following application commands: {:?}", commands);
    }

    async fn interaction_create(&self, ctx: Context, interaction: Interaction) {
        if let Interaction::ApplicationCommand(command) = interaction {
            let ctx = Arc::new(ctx);
            let command = Arc::new(command);
            let mut deferred = false;
            let command_name = command.data.name.clone();
            let content = if COMMANDS.contains_key(command_name.as_str()) {
                let ctx2 = ctx.clone();
                let command2 = command.clone();
                let c = COMMANDS.get(command_name.as_str()).unwrap();
                if c.can_execute(&ctx, &command).await {
                    let command_handle = tokio::spawn(async move {
                        c.create_interaction(&ctx2, &command2).await
                    });
                    pin_mut!(command_handle);
                    let command_result = match tokio::time::timeout(COMMAND_TIMEOUT, &mut command_handle).await {
                        Ok(result) => result,
                        Err(_) => {
                            log::warn!("Execution of '{}' takes longer than {}ms, sending deferred response", command_name, COMMAND_TIMEOUT.as_millis());
                            deferred = true;
                            if let Err(why) = command
                                .create_interaction_response(&ctx.http, |response| {
                                    response
                                        .kind(InteractionResponseType::DeferredChannelMessageWithSource)
                                })
                                .await {
                                error!("Could not send deferred response for command '{}': {:?}", command_name, why);
                                deferred = false;
                            }
                            command_handle.await
                        }
                    };

                    match command_result.map_err(|e| Error::Internal(e.to_string()).into()) {
                        Ok(Ok(Some(content))) => content,
                        Ok(Ok(None)) => "All done! :)".to_string(),
                        Err(why) | Ok(Err(why)) => {
                            error!("Failure while executing command '{}': {:?}", command_name, why);
                            format!("While trying to do that, this happened: {}", why)
                        }
                    }
                } else {
                    warn!("User '{}' is not allowed to execute command '{}'", command.user.name, command.data.name);
                    "You are not allowed to do that :(".to_string()
                }
            } else {
                error!("Command not found: {}", command_name);
                "I don't know what you mean :(".to_string()
            };
            if let Some(why) = if deferred {
                command
                    .edit_original_interaction_response(&ctx.http, |response| {
                        response
                            .content(content)
                    }).await.err()
            } else {
                command
                    .create_interaction_response(&ctx.http, |response| {
                        response
                            .kind(InteractionResponseType::ChannelMessageWithSource)
                            .interaction_response_data(|message| message.content(content))
                    })
                    .await.err()
            }
            {
                error!("Cannot respond to slash command: {}", why);
            }
        } else {
            error!("Unknown interaction type: {:?}", interaction);
        }
    }
}

fn main() {
    let args = args::Args::parse();

    simple_logger::init_with_level(args.log_level).unwrap();

    info!("Checking for youtube-dl binary: {}", &args.youtube_dl);
    match std::process::Command::new(&args.youtube_dl)
        .arg("--version")
        .stdout(Stdio::null())
        .stderr(Stdio::null())
        .stdin(Stdio::null())
        .status() {
        Ok(_) => info!("youtube-dl executed successfully"),
        Err(why) => {
            error!("youtube-dl binary '{}' not executed successfully: {}", &args.youtube_dl, why);
            std::process::exit(1)
        }
    }

    let rt = tokio::runtime::Runtime::new().unwrap();
    rt.block_on(main_async(args.shards, &args.discord_token, args.application_id, args.role_names, &args.youtube_dl));
}

async fn main_async(shards: u64, discord_token: &str, application_id: u64, allowed_roles: Vec<String>, youtube_dl: &str) {
    warn!("Starting bot with {} shard(s)", shards);
    info!("Allowed roles: {:?}", allowed_roles);

    // Login with a bot token from the environment
    let mut client = Client::builder(discord_token, GatewayIntents::from_iter([GatewayIntents::GUILDS, GatewayIntents::GUILD_VOICE_STATES]))
        .event_handler(Handler)
        .application_id(application_id)
        .register_songbird()
        .await
        .expect("Error creating client");

    {
        let mut data = client.data.write().await;
        data.insert::<AllowedRoles>(Arc::new(allowed_roles));
        data.insert::<PlayState>(Arc::new(DashMap::new()));
        data.insert::<YoutubeDl>(youtube_dl.to_string());
    }

    let shard_manager = client.shard_manager.clone();

    tokio::spawn(async move {
        match signal::exit_signal().await {
            Ok(sig) => match sig {
                Some(sig) => warn!("{} received, shutting down all shards...", sig),
                None => error!("Unable to listen for shutdown signals anymore")
            },
            Err(_) => error!("Could not setup signal handlers for shutdown")
        }

        shard_manager.lock().await.shutdown_all().await;
    });

    // start listening for events by starting the appropriate number of shards
    if let Err(why) = client.start_shards(shards).await {
        error!("An error occurred while running the client: {:?}", why);
    }
}
