FROM rust:1.68-slim-bookworm AS build

WORKDIR /build
COPY ./ /build/
RUN apt-get update && apt-get install -y build-essential cmake libopus-dev autoconf automake libtool m4 git \
    && cargo build --release

FROM debian:bookworm-slim

COPY ./docker/yt-dlp /usr/local/bin/yt-dlp

RUN apt-get update && apt-get install -y \
  curl \
  dumb-init \
  ffmpeg \
  libopus0 \
  python3 \
  && curl -L https://github.com/yt-dlp/yt-dlp/releases/latest/download/yt-dlp -o /usr/local/bin/yt-dlp-wrapped \
  && chmod a+rx /usr/local/bin/yt-dlp-wrapped /usr/local/bin/yt-dlp \
  && apt-get purge -y curl \
  && apt-get autoremove -y \
  && rm -rf /var/lib/apt/lists/*

COPY --from=build /build/target/release/vrubwoofer /usr/local/bin/vrubwoofer

ENV YTDLP=/usr/local/bin/yt-dlp-wrapped

USER nobody

ENTRYPOINT ["/usr/bin/dumb-init", "--rewrite", "15:2", "--"]

CMD ["vrubwoofer"]
