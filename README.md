# vrubwoofer

A simple Discord bot for playing audio in a voice channel.

## Discord setup

Go to the [Discord Developer Portal](https://discord.com/developers/applications) and create a new application.

For running the bot the application ID and the bot token is needed. Take the application id from this page:

![Discord: General Information](docs/discord01.png)

The bot token can be found here:

![Discord: Bot](docs/discord02.png)

## Integrating into a guild

To integrate the bot into a guild generate an Oauth url as such and use it to add the bot to the desired guild:

![Discord: OAuth2](docs/discord03.png)
